set scriptFolder=%1
set deployedFolder=%2
set resourcesFolder=%3
set mergeModuleFolder=%4
set output=%5

echo msi_package.bat

REM Cleans up potential previous output files. No message if no files match the wildcard
del /Q *.wixobj *.wixpdb 2>nul

"%WIX%\bin\candle" -ext WiXUtilExtension %scriptFolder%\*.wxs -dRedistributablesMergeModuleFolder=%mergeModuleFolder%
"%WIX%\bin\light" -ext WiXUtilExtension *.wixobj -o %output% -b %deployedFolder% -b %resourcesFolder%


