## Note: Is it still relevant with recent releases of CMake??
## CMAKE_DOCUMENTATION_START enable_cpp_version
##
## Initialize custom variables, grouped under 'OPTION', to control CMake run.
##
## CMAKE_DOCUMENTATION_END
function(ENABLE_CPP_VERSION)

    set(optionsArgs "C++11;C++14;C++17;C++20")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if (CAS_C++11)
      set(CMAKE_CXX_STANDARD 11 PARENT_SCOPE)
      set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)

      # /todo Only specify the library for version of XCode that do not use the right one by default.
      #With C++11/14 support, Xcode should use libc++ (by Clang) instead of default GNU stdlibc++
      if (CMAKE_GENERATOR STREQUAL "Xcode")
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++" PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++11" PARENT_SCOPE)
      endif ()

    elseif(CAS_C++14)
      set(CMAKE_CXX_STANDARD 14 PARENT_SCOPE)
      set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)

      if (CMAKE_GENERATOR STREQUAL "Xcode")
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++" PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++14" PARENT_SCOPE)
      endif ()

    elseif(CAS_C++17)
      set(CMAKE_CXX_STANDARD 17 PARENT_SCOPE)
      set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)

      if (CMAKE_GENERATOR STREQUAL "Xcode")
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++" PARENT_SCOPE)
          SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++17" PARENT_SCOPE)
      endif ()

    elseif(CAS_C++20)
        set(CMAKE_CXX_STANDARD 20 PARENT_SCOPE)
        set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)

        if (CMAKE_GENERATOR STREQUAL "Xcode")
            SET(CMAKE_XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES PARENT_SCOPE)
            SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++" PARENT_SCOPE)
            SET(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++20" PARENT_SCOPE)
        endif ()
    else()
        message ("enable_cpp_version() must be invoked with a valid language version.")
        return()
    endif()

endfunction()

## CMAKE_DOCUMENTATION_START cmc_all_warnings_as_errors
##
## If the function is invoked, all compilation warnings are treated as errors
##
## CMAKE_DOCUMENTATION_END
function(cmc_all_warnings_as_errors target)

    if(OPTION_DISABLE_WARNINGS_AS_ERRORS)
    # do nothing
    else()
        set(gnuoptions "AppleClang" "Clang" "GNU")
        if (CMAKE_CXX_COMPILER_ID IN_LIST gnuoptions)
            set(option "-Werror")
        elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
            set(option "-WX")
        endif()

        # Enables all warnings
        # Except on Windows, where all warnings create too many problems with system headers
        if (NOT WIN32)
            target_compile_options(${target} PRIVATE -Wall)
        endif()

        target_compile_options(${target} PRIVATE ${option})
        target_link_libraries(${target} PRIVATE ${option})
    endif()

endfunction()
