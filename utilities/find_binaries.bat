@echo off

rem declare and clear working vars
set dir_prefix=
set files=
set FILE_BUFFER=%TEMP%\%1
rem erase content of file buffer
break>%FILE_BUFFER%
call :treeProcess
goto :eof

:treeProcess
setlocal EnableDelayedExpansion
rem Do whatever you want here over the files of this subdir, for example:
for %%f in (*.dll; *.exe) do ( echo %dir_prefix%\%%f >> %FILE_BUFFER% )
for /D %%d in (*) do (
    cd %%d
    if "!dir_prefix!"=="" (set dir_prefix=%%d) else (set dir_prefix=%dir_prefix%\%%d)
    call :treeProcess
    cd ..
)

exit /b