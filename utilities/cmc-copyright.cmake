# if COPYRIGHT_YEAR is not defined in build settings, the current year will be used
function (_get_copyright_string outvar)
    if(NOT COPYRIGHT_YEAR)
        string(TIMESTAMP COPYRIGHT_YEAR "%Y")
    endif()
    if (CMAKE_SYSTEM_NAME MATCHES Darwin)
        set(${outvar} "Copyright © ${COPYRIGHT_YEAR} Astute Graphics Limited. All rights reserved." PARENT_SCOPE)
    else()
        # Preserve the Windows string which, historically went like this:
        set(${outvar} "Copyright (c) ${COPYRIGHT_YEAR} Astute Graphics Limited" PARENT_SCOPE)
    endif()
endfunction()