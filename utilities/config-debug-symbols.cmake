## CMAKE_DOCUMENTATION_START config_debug_symbols
##
## \brief Configure build to output debug symbols and install to package area
##
##
## \arg TARGET The target for which debug symbol are requested
##
## CMAKE_DOCUMENTATION_END

function(config_debug_symbols _target)

    if(CMAKE_SYSTEM_NAME MATCHES Darwin)
        set(DSYM_DEST_DIR "symbols/${CMAKE_SYSTEM_NAME}")
        install(DIRECTORY "$<TARGET_BUNDLE_DIR:${_target}>.dSYM"
            CONFIGURATIONS Release
            DESTINATION ${DSYM_DEST_DIR}
        )

        set_target_properties(${_target}
            PROPERTIES
            XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN $<IF:$<CONFIG:Debug>,NO,YES>
            XCODE_ATTRIBUTE_GCC_GENERATE_DEBUGGING_SYMBOLS "YES"
            XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT $<IF:$<CONFIG:Debug>,dwarf,dwarf-with-dsym>
        )

    endif()
endfunction()