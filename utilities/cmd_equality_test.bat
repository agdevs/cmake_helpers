set reference=%1
set value=%2
set error_message=%3

if NOT %reference%==%value% echo %error_message% && exit -2
