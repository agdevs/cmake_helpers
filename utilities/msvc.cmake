
macro(msvc_configure_runtime)
  if(MSVC)
    # Default to statically-linked runtime.
    if("${MSVC_RUNTIME}" STREQUAL "")
      set(MSVC_RUNTIME "static")
    endif()
    # Set compiler options.
    set(variables
      CMAKE_C_FLAGS_DEBUG
      CMAKE_C_FLAGS_MINSIZEREL
      CMAKE_C_FLAGS_RELEASE
      CMAKE_C_FLAGS_RELWITHDEBINFO
      CMAKE_CXX_FLAGS_DEBUG
      CMAKE_CXX_FLAGS_MINSIZEREL
      CMAKE_CXX_FLAGS_RELEASE
      CMAKE_CXX_FLAGS_RELWITHDEBINFO
      CEF_RUNTIME_LIBRARY_FLAG
    )
    if(${MSVC_RUNTIME} STREQUAL "static")
      message(STATUS
        "MSVC -> forcing use of statically-linked runtime."
      )
      foreach(variable ${variables})
        if(${variable} MATCHES "/MD")
          string(REGEX REPLACE "/MD" "/MT" ${variable} "${${variable}}")
        endif()
      endforeach()
    else()
      message(STATUS
        "MSVC -> forcing use of dynamically-linked runtime."
      )
      foreach(variable ${variables})
        if(${variable} MATCHES "/MT")
          string(REGEX REPLACE "/MT" "/MD" ${variable} "${${variable}}")
        endif()
      endforeach()
    endif()
  endif()
endmacro()

macro(msvc_enable_mp_compilation)
  #list(APPEND CMAKE_CXX_FLAGS "/MP")
  set(CMAKE_CXX_FLAGS "/MP ${CMAKE_CXX_FLAGS}")
endmacro()

# searches for a file using the globbing expression.
# returns the result if only one file is found; otherwise, returns an empty string
# if more than one, also prints a warning message
function(fileglob_one gexpr ret)
  set(${ret} "" PARENT_SCOPE)
  file(GLOB_RECURSE FLST FOLLOW_SYMLINKS ${gexpr})
  list(LENGTH FLST LSLEN)

  if(${LSLEN} GREATER 1)
    message(WARNING "File search ambiguous, expected one, found ${LLEN}")
    foreach(LENT FLST)
      message(WARNING ${LENT})
    endforeach()
    return()
  endif()

  if(${LSLEN} EQUAL 1)
    list(GET FLST 0 LOCRET)
    set(${ret} ${LOCRET} PARENT_SCOPE)
  endif()
endfunction()

# if libname_USE_PROJECT is set, locates the appropriate solution directory
# sets libname_PROJECT_FILE to be later referenced by 'include_external_msproject'
# sets libname_LIBRARY_DIR (:PATH) and libname_INCLUDE_DIRECTORIES (:LIST)
# otherwise, requires a CMake package

macro(ag_locate_library LNAME)
  # MSVC assumed
  option(${LNAME}_USE_PROJECT "Add project to the solution instead of looking for the library under the prefix" OFF)

  set(WORKSPACE_DIR CACHE PATH "Workspace path where all referenced library projects are searched")
  set(${LNAME}_SOLUTION_DIR CACHE PATH "Directory containing ${LNAME} solution")

  if(NOT ${CMAKE_GENERATOR} MATCHES "Visual Studio.*")
      if(${${LNAME}_USE_PROJECT})
        message(STATUS "${LNAME}_USE_PROJECT may not be used with a generator other than Visual Studio. Ignored.")
        set(${LNAME}_USE_PROJECT "OFF" CACHE BOOL "")
      endif()
  endif()

  if(${${LNAME}_USE_PROJECT})

      if("${WORKSPACE_DIR}" STREQUAL "")
          message(ERROR "To use project references, WORKSPACE_DIR has to be defined")
          return() # we're in a macro, so this should stop processing the enclosing file
      endif()

      if("${${LNAME}_SOLUTION_DIR}" STREQUAL "")
        message(STATUS "Searching")
        fileglob_one("${WORKSPACE_DIR}/${LNAME}.sln" SLNFILE)
        if(NOT "" STREQUAL "${SLNFILE}")
          get_filename_component(SLNDIR ${SLNFILE} DIRECTORY)
          message(STATUS "Found ${LNAME} solution at: ${SLNDIR}")
          set(${LNAME}_SOLUTION_DIR ${SLNDIR} CACHE PATH "" FORCE)
        endif()
      else()
        message(STATUS "Using previously found ${LNAME} solution location: ${${LNAME}_SOLUTION_DIR}")
      endif()

      if(NOT "" STREQUAL "${${LNAME}_SOLUTION_DIR}")
        #load the cache and retrieve other vital data
        load_cache("${${LNAME}_SOLUTION_DIR}" READ_WITH_PREFIX "AGLOC_" "${LNAME}_BINARY_DIR" "${LNAME}_SOURCE_DIR"
          "${LNAME}_INTERFACE_INCLUDE_DIRECTORIES" )
        set(${LNAME}_PROJECT_FILE "${AGLOC_${LNAME}_BINARY_DIR}/${LNAME}.vcxproj")
        message(STATUS "${LNAME} project file: ${${LNAME}_PROJECT_FILE}")
        set(${LNAME}_LIBRARY_DIR "${AGLOC_${LNAME}_BINARY_DIR}")
        message(STATUS "${LNAME} library: ${${LNAME}_LIBRARY_DIR}/$(ConfigurationName)")

        set(${LNAME}_INCLUDE_DIRECTORIES "")
        foreach(INCRELDIR "${AGLOC_${LNAME}_INTERFACE_INCLUDE_DIRECTORIES}" )
          #set(INCABSDIR "${AGLOC_${LNAME}_SOURCE_DIR}/")
          get_filename_component(INCABSDIR "${AGLOC_${LNAME}_SOURCE_DIR}/${INCRELDIR}" ABSOLUTE)
          #message(STATUS "INC DIR ${INCABSDIR}")
          list(APPEND ${LNAME}_INCLUDE_DIRECTORIES ${INCABSDIR})
        endforeach(INCRELDIR)
        message(STATUS "Include directories: ${${LNAME}_INCLUDE_DIRECTORIES}")

      endif()

  else() # ___USE_PROJECT
    message(STATUS "Looking for package ${LNAME}")
    find_package(${LNAME} REQUIRED)
  endif() # ___USE_PROJECT

endmacro()

# references a library previously located with ag_locate_library
# only to be used by application and dynamic library projects
macro(ag_reference_library TARGNAME LNAME NSPACE)

  if(${${LNAME}_USE_PROJECT})

    if("" STREQUAL "${${LNAME}_SOLUTION_DIR}")
      message(ERROR " ${LNAME}_SOLUTION_DIR not set")
      return()
    endif()

    # add .vcxproj and set includes and libraries
    message(STATUS "adding external project ${${LNAME}_PROJECT_FILE}")
    include_external_msproject(${LNAME} "${${LNAME}_PROJECT_FILE}")
    add_dependencies(${TARGNAME} ${LNAME})

    #add includes
    target_include_directories(${TARGNAME} PRIVATE "${${LNAME}_INCLUDE_DIRECTORIES}")

    target_link_libraries(${TARGNAME} "${${LNAME}_LIBRARY_DIR}/$<CONFIG>/${LNAME}$<$<CONFIG:Debug>:${CMAKE_DEBUG_POSTFIX}>.lib")
  else() # ___USE_PROJECT
    # using package location
    # ??? how to get NAMESPACE?
    message(STATUS "Using ${LNAME} package at ${${LNAME}_DIR}")
    include (${${LNAME}_DIR}/${LNAME}Targets.cmake)
    target_include_directories(${TARGNAME} PRIVATE $<TARGET_PROPERTY:${NSPACE}::${LNAME},INTERFACE_INLCUDE_DIRECTORIES>)
    target_link_libraries(${TARGNAME} ${NSPACE}::${LNAME})
  endif() # ___USE_PROJECT
endmacro()

# only set the library includes - to be used by static library projects
macro(ag_set_includes TARGETNAME LIBRARYNAME NSPACE)
  if(${${LNAME}_USE_PROJECT})

    if("" STREQUAL "${${LNAME}_SOLUTION_DIR}")
      message(ERROR "${LNAME}_SOLUTION_DIR not set")
      return()
    endif()

    #add includes
    target_include_directories(${TARGNAME} PRIVATE "${${LNAME}_INCLUDE_DIRECTORIES}")

  else() # ___USE_PROJECT

    message(STATUS "Using ${LNAME} package at ${${LNAME}_DIR}")
    include (${${LNAME}_DIR}/${LNAME}Targets.cmake)
    target_include_directories(${TARGNAME} PRIVATE $<TARGET_PROPERTY:${NSPACE}::${LNAME},INTERFACE_INLCUDE_DIRECTORIES>)

  endif() # ___USE_PROJECT
endmacro()
