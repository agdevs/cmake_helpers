## CMAKE_DOCUMENTATION_START cmc_export
##
## cmc_export is a macro wrapping the boilerplate code that is used to install an export
## created by install(TARGET ... EXPORT ...).
## See: http://www.cmake.org/cmake/help/v3.0/manual/cmake-packages.7.html#creating-packages for the source of this code
## INTERNAL_DEPENDS is a list of upstream internal targets (defined under the same root project)
## FIND_DEPENDS is a list of string that will each be used in a separate invocation of find_package
##
## CMAKE_DOCUMENTATION_END
function(cmc_export project_name)
    set(oneValueArgs "NAMESPACE")
    set(multiValueArgs "INTERNAL_DEPENDS" "FIND_DEPENDS")
    cmake_parse_arguments(CAS "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    # TARGETFILE is used in Config file as well as in this macro
    cmc_targetfile_name(${project_name} TARGETFILE)

    # Generate config files in the build tree.
    set(_invocations ${CAS_FIND_DEPENDS})
    if (CAS_INTERNAL_DEPENDS)
        list(JOIN CAS_INTERNAL_DEPENDS " " _joined_internal_depends)
        list(APPEND _invocations "${CMAKE_PROJECT_NAME} CONFIG REQUIRED COMPONENTS ${_joined_internal_depends}")
    endif()

    ## Alternative approach for internal dependencies:
    ## The modules could each be directly found in the current list dir
    #foreach (_target ${CAS_INTERNAL_DEPENDS})
    #    list(APPEND _invocations "${_target} CONFIG PATHS \$\{CMAKE_CURRENT_LIST_DIR\} NO_DEFAULT_PATH")
    #endforeach()

    _cmc_config_dependency_code(TARGET_DEPENDENCIES FIND_INVOCATIONS ${_invocations})
    configure_file(${CMAKE_SOURCE_DIR}/cmake/templates/Config.cmake.in
                   ${CMAKE_BINARY_DIR}/${project_name}Config.cmake
                   @ONLY)

    # Generates a file that will check when importing *from the build tree*
    # if the packages found by downstreams are found in the same location as they are found now
    _cmc_config_buildtree_checks_code(BUILD_TREE_DIR_CHECKS)
    configure_file(${CMAKE_SOURCE_DIR}/cmake/templates/Config-BuildTreeChecks.cmake.in
                   ${CMAKE_BINARY_DIR}/${project_name}Config-BuildTreeChecks.cmake
                   @ONLY)


    # Custom variable holding the destination folder for package configuration files
    _cmc_config_package_location(ConfigPackageLocation)

    # Install the config file (not the BuildTreeChecks.cmake, which is build tree only)
    install(FILES ${CMAKE_BINARY_DIR}/${project_name}Config.cmake
            DESTINATION ${ConfigPackageLocation})

    # Exports
    install(EXPORT ${project_name}Targets NAMESPACE "${CAS_NAMESPACE}"
            DESTINATION ${ConfigPackageLocation}) #install tree
    export(EXPORT ${project_name}Targets NAMESPACE "${CAS_NAMESPACE}" 
           FILE ${CMAKE_BINARY_DIR}/${TARGETFILE}) #build tree
endfunction()



################################################################
##
## PRIVATE
##
################################################################

macro(_cmc_config_package_location variable)
    set(${variable} "cmake/")
endmacro()

## CMAKE_DOCUMENTATION_START cmc_config_dependency_code
##
## Generate the CMake script code to find the given imported target dependencies
## This code is ready to be added to a CMake package config file.
## see: http://www.cmake.org/cmake/help/v3.3/manual/cmake-packages.7.html#creating-a-package-configuration-file
##
## CMAKE_DOCUMENTATION_END
function(_cmc_config_dependency_code output)
    set(optionsArgs "")
    set(oneValueArgs "")
    set(multiValueArgs "FIND_INVOCATIONS")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    set(_buffer "include(CMakeFindDependencyMacro)")
    foreach(dependency ${CAS_FIND_INVOCATIONS})
        string(CONCAT _buffer ${_buffer} "\n" "find_dependency(" ${dependency} ")")
    endforeach()
    if (NOT CAS_FIND_INVOCATIONS)
        string(CONCAT _buffer ${_buffer} "\n" "# No dependencies listed")
    endif()

    set(${output} ${_buffer} PARENT_SCOPE)
endfunction()


##
##
##
function(_cmc_config_buildtree_checks_code output)
    set(_check_buffer "#invokes cmc_check_import_DIR on each target imported from its build tree by the project that wrote this config.")
    foreach(dependency ${_CMC_BUILDTREE_TARGET_LIST})
        string(CONCAT _check_buffer ${_check_buffer} "\n" "    " "cmc_check_import_DIR(" ${dependency} " \"" ${${dependency}_DIR} "\")" )
    endforeach()

    set(${output} "${_check_buffer}" PARENT_SCOPE)
endfunction()

