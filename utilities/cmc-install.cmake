## CMAKE_DOCUMENTATION_START cmc_install_with_folders
##
## \brief Installs the provided files (optionally prepending DESTINATION prefix)
##        while preserving the relative path of each file (i.e. recreating folder hierarchy).
##
## \arg DESTINATION required path that will be prefixed to the provided FILES
## \arg FILES list of relative paths to files, prepended with DESTINATION when installed
##
## CMAKE_DOCUMENTATION_END
function(cmc_install_with_folders)
    set(optionsArgs "")
    set(oneValueArgs "DESTINATION")
    set(multiValueArgs "FILES")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if (NOT CAS_DESTINATION)
        message(AUTHOR_WARNING "DESTINATION value is required, skip installation for FILES")
        return()
    endif()

    foreach(_file ${CAS_FILES})
        get_filename_component(_dir ${_file} DIRECTORY)
        install(FILES ${_file} DESTINATION ${CAS_DESTINATION}/${_dir})
    endforeach()
endfunction()

## CMAKE_DOCUMENTATION_START _version_files
##
## \brief Internal helper function to generate config check files in both buid and install trees.
##
## \arg BUILDTREE_DESTINATION Path where the version config file will be created in build tree.
## \arg INSTALL_DESTINATION Path where the version config file will be installed.
##
## CMAKE_DOCUMENTATION_END
function(_version_files PACKAGE_NAME BUILDTREE_DESTINATION INSTALL_DESTINATION
                        VERSION VERSION_COMPATIBILITY ARCH_INDEPENDENT)
    if (ARCH_INDEPENDENT)
        set(_ARCH "ARCH_INDEPENDENT")
    endif()
    # Build tree
    include(CMakePackageConfigHelpers)
    write_basic_package_version_file(${BUILDTREE_DESTINATION}/${PACKAGE_NAME}ConfigVersion.cmake
                                     VERSION ${VERSION}
                                     COMPATIBILITY ${VERSION_COMPATIBILITY}
                                     ${_ARCH})
    # Install tree
    install(FILES ${BUILDTREE_DESTINATION}/${PACKAGE_NAME}ConfigVersion.cmake
            DESTINATION ${INSTALL_DESTINATION})
endfunction()

## CMAKE_DOCUMENTATION_START cmc_install_packageconfig
##
## \brief Generate a package config file for TARGET, making it available:
##        * (at configure time) in the build tree
##        * (at install time) in the install tree
##
## \arg EXPORTNAME Name of the export set for which targets files are generated.
## \arg FIND_FILE optional find file templates (see cmc_find_dependencies for syntax), invoked
##      by the generated Config file for TARGET. Allows finding external dependencies, while
##      keeping the list of said external dependencies DRY (thanks to the template re-use).
## \arg VERSION_COMPATIBILITY optional version compatibility mode for the created package.
##      Accepted values are the values for COMPATIBILITY of write_basic_package_version_file:
##      https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html#generating-a-package-version-file
##      When absent, the package version file will **not** be produced, so no version checks are made.
##      When provided, the VERSION property of TARGET will be used as package version.
## \arg ARCH_INDEPENDENT optional flag, whose presence make the package version compatible accross
##      different architectures.
##      see: https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html#command:write_basic_package_version_file
##      This flag will only have an effect if the version file is produced.
##      i.e. it should only be provided when VERSION_COMPATIBILITY is also provided.
## \arg NAMESPACE Prepended to all targets written in the export set.
##
## CMAKE_DOCUMENTATION_END
function(cmc_install_packageconfig TARGET EXPORTNAME)
    set(optionsArgs "ARCH_INDEPENDENT")
    set(oneValueArgs "NAMESPACE" "FIND_FILE" "VERSION_COMPATIBILITY")
    set(multiValueArgs "")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # suffixes with **root** project name, to group with root config in case of componentized repo
    set(_main_config_name ${CMAKE_PROJECT_NAME})
    set(_install_destination ${CMC_INSTALL_CONFIGPACKAGE_PREFIX}/${_main_config_name}/${TARGET})
    set(_buildtree_destination ${CMAKE_BINARY_DIR}/${TARGET})

    # If a find file is provided to find upstreams
    set(_findupstream_file ${TARGET}FindUpstream.cmake)
    if (CAS_FIND_FILE)
        # No value for REQUIRED and QUIET substition, to remove them
        set(find_package "find_dependency")
        set(find_internal_package "find_dependency")
        # build tree
        configure_file(${CAS_FIND_FILE} ${_buildtree_destination}/${_findupstream_file} @ONLY)
        #install tree
        install(FILES ${_buildtree_destination}/${_findupstream_file}
                DESTINATION ${_install_destination})
    endif()

    set(_targetfile "${EXPORTNAME}.cmake")

    # Generate config files in the build tree
    configure_file(${CMC_ROOT_DIR}/templates/PackageConfig.cmake.in
                   ${_buildtree_destination}/${TARGET}Config.cmake
                   @ONLY)

    # Install the config file over to the install tree
    install(FILES ${_buildtree_destination}/${TARGET}Config.cmake
            DESTINATION ${_install_destination})

    # build tree
    export(EXPORT ${EXPORTNAME}
        FILE ${_buildtree_destination}/${_targetfile}
        NAMESPACE ${CAS_NAMESPACE})

    # install tree
    install(EXPORT ${EXPORTNAME}
        FILE ${_targetfile}
        DESTINATION ${_install_destination}
        NAMESPACE ${CAS_NAMESPACE})

    #
    # Optional version logic
    #
    if (CAS_VERSION_COMPATIBILITY)
        get_target_property(_version ${TARGET} VERSION)
        if(NOT _version)
            message(SEND_ERROR "VERSION property must be set on target ${TARGET} before setting its VERSION_COMPATIBILITY")
        endif()
        _version_files(${TARGET} ${_buildtree_destination} ${_install_destination}
                       ${_version} ${CAS_VERSION_COMPATIBILITY} ${CAS_ARCH_INDEPENDENT})
    endif()
endfunction()

## CMAKE_DOCUMENTATION_START cmc_install_root_component_config
##
## \brief Generate the root package config file for a project providing several components
##        (typically, a repository containing several libraries).
##
## \arg VERSION_COMPATIBILITY optional version compatibility mode for the created package.
##      Accepted values are the values for COMPATIBILITY of write_basic_package_version_file:
##      https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html#generating-a-package-version-file
##      When absent, the package version file will **not** be produced, so no version checks are made.
##      When provided, CMAKE_PROJECT_VERSION will be used as package version.
## \arg ARCH_INDEPENDENT optional flag, whose presence make the package version compatible accross
##      different architectures.
##      see: https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html#command:write_basic_package_version_file
##      This flag will only have an effect if the version file is produced.
##      i.e. it should only be provided when VERSION_COMPATIBILITY is also provided.
##
## This config file should be found by downstream in its call to find_package(... COMPONENTS ...)
##
## CMAKE_DOCUMENTATION_END
function(cmc_install_root_component_config)
    set(optionsArgs "ARCH_INDEPENDENT")
    set(oneValueArgs "VERSION_COMPATIBILITY")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # Ensures the root config is found when looking for the name of the root project
    set(PACKAGE_NAME "${CMAKE_PROJECT_NAME}")

    # Generate root config files in the build tree
    configure_file(${CMC_ROOT_DIR}/templates/ComponentPackageRootConfig.cmake.in
                   ${CMAKE_BINARY_DIR}/${PACKAGE_NAME}Config.cmake
                   @ONLY)

    # Install the root config file over to the install tree
    install(FILES ${CMAKE_BINARY_DIR}/${PACKAGE_NAME}Config.cmake
            DESTINATION ${CMC_INSTALL_CONFIGPACKAGE_PREFIX}/${PACKAGE_NAME})

    if (CAS_VERSION_COMPATIBILITY)
        if(NOT CMAKE_PROJECT_VERSION)
            message(SEND_ERROR "Top level CMake project must have a version set before setting VERSION_COMPATIBILITY")
        endif()
        _version_files(${PACKAGE_NAME}
                       ${CMAKE_BINARY_DIR}
                       ${CMC_INSTALL_CONFIGPACKAGE_PREFIX}/${PACKAGE_NAME}
                       ${CMAKE_PROJECT_VERSION}
                       ${CAS_VERSION_COMPATIBILITY}
                       ${CAS_ARCH_INDEPENDENT})
    endif()
endfunction()

# Has to be done outside the function, otherwise returns the dir of the file *calling* the function.
set(file_dir ${CMAKE_CURRENT_LIST_DIR})

## CMAKE_DOCUMENTATION_START cmc_pack_dmg
##
## Packages the provided bundle into a dmg archive, placing the result in the same folder.
##
## CMAKE_DOCUMENTATION_END
function(cmc_pack_dmg bundle)
    install(CODE "
        include(${file_dir}/cmc-install.cmake)
        _cmc_pack_dmg(\"${bundle}\")
    ")
endfunction()


## CMAKE_DOCUMENTATION_START _cmc_pack_dmg bundle
##
## Implementation detail (not for client usage)
## Determines the output dmg path, and acts as a CMake wrapper hditutil command line execution
##
## CMAKE_DOCUMENTATION_END
function(_cmc_pack_dmg bundle)
    set(buf)
    get_filename_component(buf ${bundle} NAME_WE)
    set(output_file "${buf}.dmg")

    get_filename_component(buf ${bundle} DIRECTORY)
    set(output_file "${buf}/${output_file}")

    message(STATUS cmc_pack_dmg)
    message(STATUS "    bundle: ${bundle}")
    message(STATUS "    output: ${output_file}")

    _cmc_system_command(hdiutil create -srcfolder ${bundle} -ov ${output_file})
endfunction()

## CMAKE_DOCUMENTATION_START _cmc_system_command
##
## Implementation detail (not for client usage)
## Execute the provided system command.
## The command and its arguments has to be given as separate input parameters,
## not enclosing them in quotes.
##
## CMAKE_DOCUMENTATION_END
function(_cmc_system_command)
    set(error)
    execute_process(
        COMMAND ${ARGV}
        WORKING_DIRECTORY ${file_dir}
        ERROR_VARIABLE error
        OUTPUT_VARIABLE output
        RESULT_VARIABLE res
    )

    if (output)
        message(STATUS "Execution output: ${output}")
    endif()

    if (NOT res EQUAL 0)
        message(FATAL_ERROR "Error in command.\n"
            "    Result: ${res}\n"
            "    Error: ${error}"
        )
    endif()
endfunction()

## CMAKE_DOCUMENTATION_START cmc_strip_installed_binaries_macos
##
## \brief Strip the binaries contained in the list during the install step
##
## Only for the macOS/Darwin platform
##
## CMAKE_DOCUMENTATION_END
function(cmc_strip_installed_binaries_macos)
    if (NOT CMAKE_SYSTEM_NAME MATCHES Darwin)
        message (FATAL_ERROR "Platform ${CMAKE_SYSTEM_NAME} not supported - this is a macOS specific function!")
    endif()
    foreach (_bin ${ARGV})
        install(CODE "
                message (STATUS \"STRIP: ${_bin}\")
                execute_process(
                    COMMAND ${CMAKE_STRIP} -S -x -no_code_signature_warning \"${_bin}\"
                )
        ")
    endforeach()
endfunction()

## CMAKE_DOCUMENTATION_START cmc_sign_installed_binaries_macos
##
## \brief Sign the binaries contained in the list during the install step
##
## Only for the macOS/Darwin platform.
## Note that the binaries may already have been signed during the build step and a warning might be issued.
##
## CMAKE_DOCUMENTATION_END
function(cmc_sign_installed_binaries_macos)
    if (NOT CMAKE_SYSTEM_NAME MATCHES Darwin)
        message (FATAL_ERROR "Platform ${CMAKE_SYSTEM_NAME} not supported - this is a macOS specific function!")
    endif()
    foreach (_bin ${ARGV})
        install(CODE "
                message (STATUS \"SIGN: ${_bin}\")
                execute_process(
                    COMMAND codesign --force --sign \"Developer ID Application: Astute Graphics Limited (TMHZM29UJD)\" \"${_bin}\"
                )
        ")
    endforeach()
endfunction()

## CMAKE_DOCUMENTATION_START cmc_target_package_msi
##
## Creates a target named $added_target_name, after ensuring that DEPENDS are met.
## When the target is built, it packages the target application into an .msi installer.
## The packaging procedure relies on Wix toolset, and is driven by custom .wsx files,
## that must be located under ${CMAKE_CURRENT_BINARY_DIR}.
## Additional resources files can be made accessible by storing them in ${CMAKE_CURRENT_SOURCE_DIR}/resources/.
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_add_package_target added_target_name)
    set(optionsArgs "REQUIRED")
    set(oneValueArgs "OUTPUT" "WORKING_DIRECTORY")
    set(multiValueArgs "DEPENDS")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    _cmc_msi_pipeline_deploy_dir(_deployed_dir)

    cmc_assign_vcredistributables_mergemodule_folder()
    if (NOT VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER)
        cmc_target_failure(${added_target_name} "VC Redistributables merge modules are not located"
                           REQUIRED ${CAS_REQUIRED})
    endif()

    execute_process(COMMAND "${file_dir}/test_wix.bat" OUTPUT_VARIABLE _candle_output)
    if(NOT _candle_output MATCHES "version 3\\.11")
        cmc_target_failure(${added_target_name} "Wix 3.11.x installation was not found behind the %wix% env var"
                           REQUIRED ${CAS_REQUIRED})
    endif()

    set(output ${CAS_OUTPUT})
    add_custom_command(OUTPUT ${output}
                       COMMAND ${file_dir}/cmd_equality_test.bat Release $<CONFIG> "ERROR: Only RELEASE builds can be packaged"
                       COMMAND ${file_dir}/msi_package.bat "./"
                                                           "${_deployed_dir}"
                                                           "${CMAKE_CURRENT_SOURCE_DIR}/resources"
                                                           "${VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER}"
                                                           ${CMAKE_CURRENT_BINARY_DIR}/${output}
                       WORKING_DIRECTORY ${CAS_WORKING_DIRECTORY}
                       DEPENDS ${CAS_DEPENDS}
    )

    add_custom_target(${added_target_name} ALL DEPENDS ${output})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${output} DESTINATION "distribution")
endfunction()


## CMAKE_DOCUMENTATION_START cmc_msi_pipeline_add_archive_packager_target
##
## Creates a target named $added_target_name.
##
## At CMake "generate" time, it first copies to the working directory
## every resource and merge folder required to assemble the msi installer,
## and also create a "prepare_installer.bat" file to drive the whole packaging procedure.
## This way, the working directory is standalone, allowing to make the msi installer only with its files.
##
## When the added target is built, it archive the whole working directory,
## so it can more easily be moved to another environment with Wix installed.
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_add_archive_packager_target added_target_name)
    set(optionsArgs "REQUIRED")
    set(oneValueArgs "OUTPUT" "WORKING_DIRECTORY" "MSI_FILE")
    set(multiValueArgs "DEPENDS")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    cmc_assign_vcredistributables_mergemodule_folder()
    if (NOT VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER)
        cmc_target_failure(${added_target_name} "VC Redistributables merge modules are not located"
                           REQUIRED ${CAS_REQUIRED})
    endif()

    _cmc_msi_pipeline_deploy_dir(_deployed_dir)
    set(resources_dir "resources")
    set(mergemodules_dir "merge_modules")

    #
    # Copy different resources located on the build machine into the working directory
    #
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/resources ${file_dir}/msi_package.bat
         DESTINATION ${CAS_WORKING_DIRECTORY})

    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/resources ${file_dir}/find_binaries.bat
         DESTINATION ${CAS_WORKING_DIRECTORY})

    file(COPY ${VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER}/${VC_REDISTRIBUTABLES_MERGE_FILE}
         DESTINATION ${CAS_WORKING_DIRECTORY}/${mergemodules_dir})

    set(output_msi ${CAS_MSI_FILE})
    configure_file(${file_dir}/prepare_installer.bat.in ${CAS_WORKING_DIRECTORY}/prepare_installer.bat)

    #
    # Creates a target to archive everything to be able to run Wix on another environment
    #
    set(output_archive ${CAS_OUTPUT})
    add_custom_command(OUTPUT ${output_archive}
                       COMMAND ${file_dir}/cmd_equality_test.bat Release $<CONFIG> "ERROR: Only RELEASE builds can be packaged"
                       COMMAND ${CMAKE_COMMAND} -E tar c ${output_archive} --format=zip -- ${CAS_WORKING_DIRECTORY}
                       DEPENDS ${CAS_DEPENDS}
    )

    add_custom_target(${added_target_name} ALL DEPENDS ${output_archive})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${output_archive} DESTINATION "distribution")
endfunction()

## CMAKE_DOCUMENTATION_START _cmc_msi_pipeline_deploy_dir
##
## Writes the hardcoded deploy directory to the variable given as $deploy_dir
##
## CMAKE_DOCUMENTATION_END
macro(_cmc_msi_pipeline_deploy_dir deploy_dir)
    set(${deploy_dir} "deployed_files")
endmacro()


## CMAKE_DOCUMENTATION_START cmc_msi_pipeline_copy_targets
##
## Create a custom command that copies the binaries for all targets provided to TARGETS
## to the deploy directory (given by _cmc_msi_pipeline_deploy_dir)
## The command output is given as OUTPUT argument, and it DEPENDS on all provided targets
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_copy_targets)
    set(optionsArgs "")
    set(oneValueArgs "OUTPUT" "WORKING_DIRECTORY")
    set(multiValueArgs "TARGETS")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    foreach(_target ${CAS_TARGETS})
        list(APPEND _binaries $<TARGET_FILE:${_target}>)
    endforeach()

    _cmc_msi_pipeline_deploy_dir(_deployed_dir)
    # Copy the executables into it
    # Do not use add_custom_command here, since it will issue a warning that no output were created.
    add_custom_target(${CAS_OUTPUT}
                      COMMAND ${CMAKE_COMMAND} -E copy ${_binaries} ${_deployed_dir}
                      WORKING_DIRECTORY ${CAS_WORKING_DIRECTORY}
                      DEPENDS ${CAS_TARGETS}
    )
endfunction()

## CMAKE_DOCUMENTATION_START cmc_msi_pipeline_copy_targets
##
## Create a custom command that copies the binaries for all targets provided to TARGETS
## to the deploy directory (given by _cmc_msi_pipeline_deploy_dir)
## The command output is given as OUTPUT argument, and it DEPENDS on all provided targets
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_copy_items)
    set(optionsArgs "")
    set(oneValueArgs "SOURCE_DIR" "OUTPUT" "WORKING_DIRECTORY")
    set(multiValueArgs "TARGETS")
    cmake_parse_arguments(CAS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    _cmc_msi_pipeline_deploy_dir(_deployed_dir)
    # Creates a destination directory for deployed files (first removing the directory if it already exist)
    # Copy the executables into it
    # Do not use add_custom_command here, since it will issue a warning that no outputs were created.
    add_custom_target(${CAS_OUTPUT}
                       COMMAND ${CMAKE_COMMAND} -E copy_directory ${CAS_SOURCE_DIR} ${_deployed_dir}
                       WORKING_DIRECTORY ${CAS_WORKING_DIRECTORY}
                       DEPENDS ${CAS_TARGETS}
                       COMMENT "Copy ${CAS_SOURCE_DIR} to ${CAS_WORKING_DIRECTORY}/${_deployed_dir}"
    )
endfunction()


## CMAKE_DOCUMENTATION_START cmc_assign_vcredistributables_mergemodule_folder
##
## Takes a guess to locate the Visual Studio 2022 redistributables merge module folder, if it is not already set.
## Stores the result in VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER cache variable.
##
## CMAKE_DOCUMENTATION_END
function(cmc_assign_vcredistributables_mergemodule_folder)
    if(NOT VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER)
        # Attempt at an educated guess
        file(GLOB _vs_mergemodules
             # Local, commented out to avoid Azure taking the folder ending 14.16.27012/MergeModules\Microsoft_VC142_CRT_x86.msm
             # it does not contain a Microsoft_VC142_CRT_x86.msm, but a Microsoft_VC141_CRT_x86.msm
             #"C:/Program Files (x86)/Microsoft Visual Studio/2019/*/VC/Redist/MSVC/14.*.*/MergeModules"
             # Azure CI, takes precedence if found
             "C:/Program Files/Microsoft Visual Studio/2022/*/VC/Redist/MSVC/v143/MergeModules"
        )

        list(LENGTH _vs_mergemodules _vsmm_length)
        if(_vsmm_length)
            # Get the last element
            list(GET _vs_mergemodules -1 _vsmm)
		message(warning "=== vssm ${_vsmm}")
        endif()
        set(VC_REDISTRIBUTABLES_MERGEMODULE_FOLDER ${_vsmm} CACHE
            PATH "The *folder* containing VS 2022 Redistributables merge modules"
            FORCE
        )
    endif()
endfunction()

## CMAKE_DOCUMENTATION_START cmc_target_failure
##
## When called, will interrupt processing current function or CMake file
## If (optional) one value argument REQUIRED is set to true, the message is displayed and generation canceled
## If (global) OPTION_VERBOSE_ADD_SUBDIR is True, will say the target is not added because of reason
##
## CMAKE_DOCUMENTATION_END
macro(cmc_target_failure target_name reason)
    set(oneValueArgs "REQUIRED")
    cmake_parse_arguments(CAS "" "${oneValueArgs}" "" ${ARGN})

    set(_message "target '${target_name}' will NOT be added because ${reason}")
    if(${CAS_REQUIRED})
        message(SEND_ERROR "Required ${_message}")
    elseif(${OPTION_VERBOSE_ADD_SUBDIR})
        message(STATUS ${_message})
    endif()
    unset(_message)
    return()
endmacro()


## CMAKE_DOCUMENTATION_START _cmc_msi_pipeline_generate_wxs
##
## Create wsx file (wix merge module)
## The command output is given as OUTPUT argument
##
## CMAKE_DOCUMENTATION_END
function(_cmc_msi_pipeline_generate_wxs )

set(optionsArgs "")
set(oneValueArgs "CEF_ROOT" "CEF_VERSION" "DESTINATION" "OUTPUT" "WORKING_DIRECTORY")
set(multiValueArgs "TARGETS")
cmake_parse_arguments(WXS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

add_custom_target(${WXS_OUTPUT}
    COMMAND ${file_dir}/gen_wxs_remove_unwanted_files.bat "./"
    COMMAND ${file_dir}/gen_wxs.bat "./"
                                    "${WXS_DESTINATION}"
                                    "${WXS_CEF_VERSION}"
    WORKING_DIRECTORY ${WXS_WORKING_DIRECTORY}
    DEPENDS ${WXS_TARGETS}
    COMMENT "Creating ${WXS_DESTINATION}/cef_${WXS_CEF_VERSION}.wxs file."
    )

endfunction()

## CMAKE_DOCUMENTATION_START cmc_msi_pipeline_prepare_and_generate_merge_module
##
# The wix module file ${cef_wxs_file} is expected in the current source dir.
# If it does not exist it will be generated, this may be the case for managing version updates of cef
# Step 1. Gather all required cef binaries and resources + License in folder _cef_wxs_gen_workdir
# Step 2. calling wix heat to generate the wxs file
# Step 3. Manually review the wxs file commit it to source control.
# Step 4. Re-run the cmake or conan build step to take the wxs file into account to allow the msi file to be generated.
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_prepare_and_generate_merge_module)
    set(optionsArgs "")
    set(oneValueArgs "CEF_ROOT" "CEF_VERSION" "DESTINATION" "OUTPUT" "WORKING_DIRECTORY")
    set(multiValueArgs "TARGETS")
    cmake_parse_arguments(WXS "${optionsArgs}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )


    message (WARNING "${cef_wxs_file} does not exist, so we create it in \
    ${WXS_DESTINATION}. Make sure to review and commit it to source control.")

    set (_cef_wxs_gen_workdir "cef_wxs_gen_workdir")
    file(REMOVE_RECURSE "${WXS_WORKING_DIRECTORY}/${_cef_wxs_gen_workdir}")
    _cmc_msi_pipeline_deploy_dir(_deployed_dir)
    configure_file(${WXS_CEF_ROOT}/LICENSE.TXT ${_cef_wxs_gen_workdir}/${_deployed_dir}/LICENSE.TXT COPYONLY)

    cmc_msi_pipeline_copy_items(
        TARGETS copy_targets_tmp
        SOURCE_DIR "${WXS_CEF_ROOT}/Release/"
        OUTPUT copy_cef_binaries_wxs_gen
        WORKING_DIRECTORY ${_cef_wxs_gen_workdir})

    cmc_msi_pipeline_copy_items(
        TARGETS copy_cef_binaries_wxs_gen
        SOURCE_DIR "${WXS_CEF_ROOT}/Resources/"
        OUTPUT copy_cef_resources_wxs_gen
        WORKING_DIRECTORY ${_cef_wxs_gen_workdir})

    _cmc_msi_pipeline_generate_wxs(
        TARGETS copy_cef_resources_wxs_gen
        CEF_ROOT ${WXS_CEF_ROOT}
        CEF_VERSION ${WXS_CEF_VERSION}
        DESTINATION ${WXS_DESTINATION}
        OUTPUT cef_merge_module
        WORKING_DIRECTORY ${_cef_wxs_gen_workdir}/${_deployed_dir})

    # always run
    add_custom_target(_build_wxs ALL DEPENDS cef_merge_module)

endfunction()

## CMAKE_DOCUMENTATION_START cmc_msi_pipeline_gather_cef_license
##
##
##
## CMAKE_DOCUMENTATION_END
function(cmc_msi_pipeline_gather_cef_license CEF_ROOT)
    _cmc_msi_pipeline_deploy_dir(_deployed_dir)
    configure_file(${CEF_ROOT}/LICENSE.TXT ${_package_workdir}/${_deployed_dir}/LICENSE.TXT COPYONLY)
endfunction()
